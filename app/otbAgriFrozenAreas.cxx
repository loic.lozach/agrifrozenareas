#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbAFASource.h"
#include "otbStreamingStatisticsMapFromLabelImageFilter.h"

// csv	
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <iterator>
#include <string>
#include <algorithm>
#include <numeric> 
#include <math.h>

namespace otb
{
namespace Wrapper
{
class AgriFrozenAreas : public otb::Wrapper::Application
{
public:
  typedef AgriFrozenAreas Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(AgriFrozenAreas, otb::Wrapper::Application);
  
  
  
  /** Typedefs for image concatenation */
  typedef TensorflowSource<FloatVectorImageType>                       TFSourceType;
  /** Statistics */
  typedef otb::StreamingStatisticsMapFromLabelImageFilter<FloatVectorImageType, UInt32ImageType> StatsFilterType;
  typedef itk::ImageRegionIterator<FloatVectorImageType> IteratorType;
  typedef itk::ImageRegionConstIterator<FloatVectorImageType> ConstIteratorType;
  

private:
  void DoInit() override
  {
    SetName("AgriFrozenAreas");
    SetDescription("AgriFrozenAreas.");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loic Lozach");
    SetDocSeeAlso(" ");

     // Input images
    AddParameter(ParameterType_InputImageList,"ilsigma",    "Input Sentinel-1 time series");
    SetParameterDescription                  ("ilsigma",    "Input SAR sigma0 calibrated image list");
    AddParameter(ParameterType_InputImageList,"iltemp",     "Input ERA5-T temperature image list");
    SetParameterDescription                  ("iltemp",     "Input ERA5-T images with dates corresponding to S1 acquisitions");
    AddParameter(ParameterType_StringList,    "datelist",   "Ordered date list corresponding to input images (YYYYMMDDTHHMMSS)");
    SetParameterDescription                  ("datelist",   "Ordered date list corresponding to input images or text file containing it");
    AddParameter(ParameterType_InputImage,    "inlabels",   "Input labels image");
    SetParameterDescription                  ("inlabels",   "Input labels image corresponding to parcels id");
    AddParameter(ParameterType_Int,           "nodatalabel","Input label image no-data value");
    SetDefaultParameterInt("nodatalabel", 0);
    // Output image
    AddParameter(ParameterType_Group,         "out",       "Output files paramaters group");
    AddParameter(ParameterType_OutputFilename,   "out.sigma", "Output mean sigma csv file");
    SetParameterDescription                  ("out.sigma", "Output mean sigma per image and parcels csv file");
    AddParameter(ParameterType_OutputFilename,   "out.temp", "Output mean temperature csv file");
    SetParameterDescription                  ("out.sigma", "Output mean temperature per image and parcels csv file");
   
  }

  void DoUpdateParameters() override
  {
      
      m_SarList = FloatVectorImageListType::New();
      m_EraList = FloatVectorImageListType::New();
      
  }
  
  /*
   * Remove the entry of the map corresponding to the no data label value
   */
  template<class TMap>
  void RemoveNoDataLabelFromMap(TMap& map)
  {
    int intNoData = GetParameterInt("nodatalabel");
    map.erase(intNoData);
  }
  
  template<class TMap>
  void RemoveNoDataLabelFromMap(TMap& map, std::vector<long> labelstoerase)
  {
    for (auto& lte: labelstoerase){
        map.erase(lte);
    }
  }
  
  template<class TMap>
  void ExportMapToCsv(TMap& map, std::vector<std::string> csvheader, std::string output)
  {
     std::fstream file;
    file.open(output, std::ios::out | std::ios::trunc);

    
    // Iterate over the range and add each lement to file seperated by delimeter.
    for (std::vector<std::string>::iterator ith=csvheader.begin(); ith != csvheader.end(); ++ith)
    {
            file << *ith;
            if (ith != csvheader.end())
                    file << ";";
    }
    file << "\n";
    

    for (auto& entry: map){
        // Adding vector to CSV File
        itk::VariableLengthVector<double> vecteur = entry.second;
        std::vector<std::string> dataList_1 = { std::to_string(entry.first)};
        for( unsigned int i = 0; i < vecteur.Size(); i++ ){
            dataList_1.push_back(std::to_string(vecteur.GetElement(i)));
        }
        
        for (std::vector<std::string>::iterator itl=dataList_1.begin(); itl != dataList_1.end(); ++itl)
        {
                file << *itl;
                if (itl != dataList_1.end())
                        file << ";";
        }
        file << "\n";
    }
    // Close the file
    file.close();
  }

  std::string getFileName(const std::string& s) {

    char sep = '/';


    std::size_t i = s.rfind(sep, s.length());
    if (i != std::string::npos) {
        return(s.substr(i+1, s.length() - (i+1)));
    }

    return("");
  }
  
  boost::posix_time::ptime getDateFromFileName(const std::string& s, const std::string& sar_or_era) {
    
    char tsep = 'T';
    std::vector<std::string> result; 
    boost::split(result, s, boost::is_any_of("_"));
    if( sar_or_era == "sar"){
        std::size_t i = result[4].rfind(tsep, result[4].length());
        otbAppLogINFO("Converting " << (result[4]));
        if (i != std::string::npos and i == 8) {
            return(boost::posix_time::from_iso_string(result[4]));
        }
        
    }else if( sar_or_era == "era"){
        std::string era_ext = result[result.size()-1];
        std::string era_d = era_ext.substr(0, era_ext.length() - 5);
        std::size_t i = era_d.rfind(tsep, era_d.length());
        otbAppLogINFO("Converting " << (era_d));
        if (i != std::string::npos and i == 8) {
            return(boost::posix_time::from_iso_string(era_d));
        }
    }
    
    
    

    return(boost::posix_time::from_iso_string(result[4]));

  }

  void DoExecute() override
  {  
    using namespace boost::posix_time;
    
//    std::vector<std::string> sarfiles;
    std::vector<std::string> datelist = GetParameterStringList("datelist");
    if(datelist.size() == 1) {
        //read text file
    }else if(datelist.size() != GetParameterStringList("iltemp").size()) {
        otbAppLogFATAL("Error : Input SAR files number doesn't match input date list");
    }
    if(GetParameterStringList("ilsigma").size() != GetParameterStringList("iltemp").size()) {
        otbAppLogFATAL("Error : Input SAR files number and Temperature files number don't match!");
    }
    
    // Getting image dates and sorting by dates
    otbAppLogINFO("Checking and sorting input images by dates. count = " );
    
    
    ptime sardate;
    std::vector<ptime> timeseries;
    // Create an ordered stack of input images coherent with csv
    for( unsigned int i = 0; i < datelist.size(); i++ ){
        otbAppLogINFO("Get date for " << (datelist[i]));
        sardate = from_iso_string(datelist[i]);
        if(sardate.is_not_a_date_time()){
            otbAppLogFATAL("Can't convert to date from date list");
        }
        timeseries.push_back(sardate);         
    }
    
      
    for( unsigned int j = 0; j < GetParameterImageList("ilsigma")->Size(); j++ ){
        m_SarList->PushBack(GetParameterImageList("ilsigma")->GetNthElement(j));
        m_EraList->PushBack(GetParameterImageList("iltemp")->GetNthElement(j));
    }

    m_SarSource.Set(m_SarList);
      
    // Compute stats
    int intNoData = GetParameterInt("nodatalabel");
    std::cout << "Not Using Nodata = "  << std::to_string(intNoData) << " in statfilter" << std::endl;
    m_SarStatsFilter = StatsFilterType::New();
    m_SarStatsFilter->SetUseNoDataValue(true);
    m_SarStatsFilter->SetNoDataValue(0);
    m_SarStatsFilter->SetInput(m_SarSource.Get());
    m_SarStatsFilter->SetInputLabelImage(GetParameterUInt32Image("inlabels"));
    
    AddProcess(m_SarStatsFilter->GetStreamer(), "Computing statistics");
    m_SarStatsFilter->Update();
    
     m_EraSource.Set(m_EraList);
      
    // Compute stats
    std::cout << "Not Using Nodata = "  << std::to_string(intNoData) << " in statfilter" << std::endl;
    m_EraStatsFilter = StatsFilterType::New();
    m_EraStatsFilter->SetUseNoDataValue(true);
    m_EraStatsFilter->SetNoDataValue(0);
    m_EraStatsFilter->SetInput(m_EraSource.Get());
    m_EraStatsFilter->SetInputLabelImage(GetParameterUInt32Image("inlabels"));
    
    AddProcess(m_EraStatsFilter->GetStreamer(), "Computing statistics");
    m_EraStatsFilter->Update();

    StatsFilterType::PixelValueMapType meanSarValues = m_SarStatsFilter->GetMeanValueMap();
    RemoveNoDataLabelFromMap<StatsFilterType::PixelValueMapType>(meanSarValues);
    otbAppLogINFO("Done (" << (meanSarValues.size()) << " objects)");
    
    StatsFilterType::PixelValueMapType meanEraValues = m_EraStatsFilter->GetMeanValueMap();
    RemoveNoDataLabelFromMap<StatsFilterType::PixelValueMapType>(meanEraValues);
    otbAppLogINFO("Done (" << (meanEraValues.size()) << " objects)");
    
    
    otbAppLogINFO("Exporting freeze detect to csv file");
        	// Creating an object of CSVWriter
   
    std::vector<std::string> dataHeader = { "Labelid"};
    for( unsigned int i = 0; i < timeseries.size() ; i++ ){
        dataHeader.push_back(to_iso_string(timeseries[i]));
    }
    otbAppLogINFO("Exporting mean temperature to csv file");
        	// Creating an object of CSVWriter
    std::string outfilename = this->GetParameterAsString("out.temp");
    ExportMapToCsv<StatsFilterType::PixelValueMapType>(meanEraValues, dataHeader, outfilename);
    
    otbAppLogINFO("Exporting mean sigma0(40) to csv file");
        	// Creating an object of CSVWriter
    outfilename = this->GetParameterAsString("out.sigma");
    ExportMapToCsv<StatsFilterType::PixelValueMapType>(meanSarValues, dataHeader, outfilename);
    
  }

  // Stack
  FloatVectorImageListType::Pointer m_SarList;
  FloatVectorImageListType::Pointer m_EraList;
  TFSourceType      m_SarEraSource;
  TFSourceType      m_SarSource;
  TFSourceType      m_EraSource;
  // Compute a std::map<labels, S1 pixels>
  StatsFilterType::Pointer m_SarStatsFilter;
  StatsFilterType::Pointer m_EraStatsFilter;
  
  
};
}
}
OTB_APPLICATION_EXPORT(otb::Wrapper::AgriFrozenAreas)
